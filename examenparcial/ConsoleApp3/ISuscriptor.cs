﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp3
{
    interface ISuscriptor
    {
        // La creamos para luego ayudarnos mostrando si 
        // el programa esta guardando grabacion
        void Renovar (String Mensaje);
        
    }
}
