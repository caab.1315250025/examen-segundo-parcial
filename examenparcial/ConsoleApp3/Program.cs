﻿using System;

namespace ConsoleApp3
{
    class Program
    {
        static void Main(string[] args)
        {
            //Creamos al sujeto Camara como "hikvision"

            Camaras hikvision = new Camaras();

            //Creamos una lista de clientes 
            Suscriptor cliente1 = new Suscriptor("Gustavo", hikvision);
            Suscriptor cliente2 = new Suscriptor("Joffre", hikvision);
            Suscriptor cliente3 = new Suscriptor("Axel", hikvision);


            //Procedemos a hacer la simulacion de movimiento
            for (int i = 0; i < 10; i++)
            {
                hikvision.AlmacenarVideo();
            }

            //Si alguno desea desuscribirse o salir 
            Console.WriteLine("------Elimando Cliente Axel-------");
            hikvision.Elimina(cliente3);

            for (int i = 0; i < 10; i++)
            {
                hikvision.AlmacenarVideo();
            }

            //Si alguno otro cliente desea desuscribirse o salir 
            Console.WriteLine("------Elimando Cliente Gustavo-------");
            hikvision.Elimina(cliente1);

            for (int i = 0; i < 10; i++)
            {
                hikvision.AlmacenarVideo();
            }

        }
    }
}
