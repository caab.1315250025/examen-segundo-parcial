﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp3
{
    interface ISujeto
    {      
        //Propiedades
        void Agrega (ISuscriptor suscriptor);
        void Elimina (ISuscriptor suscriptor);

        //Metodos
        void Alarma ();        
    }
}
