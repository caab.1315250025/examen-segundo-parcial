﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp3
{
    class Suscriptor : ISuscriptor
    {
        //Creamos las variables nombre para ingresar cada uno en cliente
        string Nombre;
        // Variable referente camara_1 que es de la CLASE referente Camaras
        Camaras camara_1;
        //Constructor para Observador con parametros nombre del usuario y camaras que en este caso seria para las Camaras
        // Al final agregamos la lista de clientes que van a observar cualquier grabacion almacenada
        public Suscriptor(String tunombre, Camaras tucamaras)
        {
            Nombre = tunombre;
            camara_1 = tucamaras;
            camara_1.Agrega(this);
        }

        // Nos alerta cualquier cambio realizado con la informacion completa.
        public void Renovar(string Mensaje)
        {
            Console.WriteLine("{0},{1}", Nombre, Mensaje);
        }
    }
}
