﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp3
{
    class Camaras : ISujeto
    {
        string Mensaje; 

        //Crear lista para guardar a los clientes
        IList<ISuscriptor> suscriptors = new List<ISuscriptor>();

        //Creamos una variable random para obtener resultados aleatorios en la simulacion
        private Random random = new Random();
        int a;
        public int A { get => a; set => a = value; }

        //Añadimos un Cliente a la lista de los clientes suscriptores
        public void Agrega(ISuscriptor suscriptor)
        {
            suscriptors.Add(suscriptor);
        }

        //Eliminamos un Cliente de la lista de los clientes suscriptores 
        public void Elimina(ISuscriptor suscriptor)
        {
            suscriptors.Remove(suscriptor);
        }

        //Recibimos una alerta del nuevo estado de todos los clientes
        public void Alarma()
        {
            foreach (ISuscriptor item in suscriptors)
            {
                item.Renovar(Mensaje);

            }

        }
        
        // Simulamos un almacenamiento de grabacion con variables random para cambiar el estado de los clientes
        // una vez por semana
        public void AlmacenarVideo()
        {
            a = random.Next(25);
            if (a %7 == 0)
            {
                Console.WriteLine("------Una semana de grabacion------");
                Mensaje = "La grabacion se esta almacenando";

                Alarma();
            }
                
        }
    }
}
